/* main.c simple program to test assembler program */

#include <stdio.h>
#include <stdint.h>
#include <stdint-gcc.h>
extern long long int test(long long int a, long long int b);
extern uint64_t multiply(uint32_t a, uint32_t b);
extern void swap(long long int * p1 , long long int * p2);

void testSwap(void)
{
	int a = 5, b = 10;
	printf("a: %d, b: %d\n", a, b);
	swap(&a, &b);
	printf("Swapped value of a with b.\n");
	printf("a: %d, b: %d\n", a, b);
}

void testMultiply(void)
{
	uint64_t a = multiply(0, 0);
	printf(" 0 * 0 :%u\n", a);
	uint64_t b = multiply(0, 1);
	printf(" 0 * 1 :%u\n", b);
	uint64_t c = multiply(1, 0);
	printf(" 1 * 0 :%u\n", c);
	uint64_t d = multiply(1, 1);
	printf(" 1 * 1 :%u\n", d);
	uint64_t e = multiply(1, 4294967295);
	printf(" 1 * 4294967295 :%u\n", e);
	uint64_t f = multiply(4294967295, 1);
	printf(" 4294967295 * 1 :%u\n", f);
	uint64_t g = multiply(4294967295, 4294967295);
	printf(" 4294967295 * 4294967295 :%u\n", g);
}

int main(void)
{
	testSwap();
	testMultiply();
    return 0;
}
